
Apologies for the late response - it’s been quite hectic at work and I appreciate your patience!

We’re looking for a basic tool or script that someone with only basic technical skills can run. The script or tool will do the below operations in order:

1). Take input of a simple text file that contains a list of instagram urls.
As an example, the text file may contain a list of links like this:

https://instagram.com/p/BKMJLDQBpkn/
https://instagram.com/p/BKJE8QSjZua/
https://instagram.com/p/BKIpRBWBnv7/

2). Scrape the media for each posting (image or video).

3). Scrape the Instagram account name of the poster and scrape all the text in the caption of the post.

4). Now the tool will automatically take the scraped image or video and caption text from each posting and upload them to separate Buffer queues for each link in the list.

5). The scraped caption and account name should be reformatted with added text and placed in the Buffer queue like this:

By @<scraped instagram account name>
<scraped caption text>
Follow ?????? @artnerd.s
-
-
-
#artnerd.s #vsco #vscoart #dscolor #livecolorfully 
#contemporaryart #interiordesign #design #homedecor #dailyart 
#artdaily #homedesign #illustration #drawing #graphic #copic 
#illustagram #artstagram #artgram #artwork #animation 
#2danimation

6). As an example, the caption for this Instagram link https://instagram.com/p/BKMJLDQBpkn/ should be reformatted to look like this:

By @annettelabedzki
BIG BLUE EYEBALL
Follow ?????? @artnerd.s
-
-
-
#artnerd.s #vsco #vscoart #dscolor #livecolorfully 
#contemporaryart #interiordesign #design #homedecor #dailyart 
#artdaily #homedesign #illustration #drawing #graphic #copic 
#illustagram #illust #artstagram #artgram #artwork #animation 
#2danimation

7). The tool will perform the above operations for each link in the text file. You’ll also need to open up a free account at Buffer.com to test your tool.

Ideally this tool/script can be run on a Mac but a PC will be fine too. 

Let me know if you’d be interested in building this. Hopefully this is clear but please don’t hesitate to contact me if you have any further questions!

Cheers,
Daniel