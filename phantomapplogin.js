var page = new WebPage(),
    stepIndex = 0,
    resultList = [],loginInfoList=[],messageFormat={},
    globalTimeout = 30000,
    currentRowIndex = 0;
loadInProgress = false;



var fs = require('fs');
var content = fs.read('./src/configuration/BufferLoginInfo.txt');
loginInfoList = content.split("\n"); 
messageFormat = fs.read('./src/configuration/BufferPostInfo.txt');
content = fs.read('./src/configuration/TimerInfo.txt');
TimerInfo = content.split("\n"); 
globalTimeout =parseInt( TimerInfo[0]) * 1000; 
console.log("timer in seconds" ,    TimerInfo[0]  );

page.onConsoleMessage = function(msg) {
    // console.log(msg);
};

page.onLoadStarted = function() {
    loadInProgress = true;
    console.log("LOAD STARTED");
};

page.onLoadFinished = function() {
    loadInProgress = false;
    console.log("LOAD FINISHED ");
};
var formClassName = "myForm";
var steps = [
    function() {
        //STEP1 -LOAD HOME LOGIN PAGE

        console.log('LOAD HOME LOGIN PAGE ');
         
        page.open("http://www.buffer.com");
    },
    function() {
        //STEP2 - PERFORM LOGIN

        console.log('PERFORM LOGIN  ');
        page.injectJs('jquery');
 
        //Enter Credentials
        page.evaluate(function(loginInfoList) {

            // console.log(document.querySelectorAll('html')[0].outerHTML);


            $('#modal-login-only').reveal();


            $('div#modal-login-only input#email').focus();
            $('div#modal-login-only input#email').val(loginInfoList[0]);

            $('div#modal-login-only input#password').focus();
            $('div#modal-login-only input#password').val(loginInfoList[1]);


            $('div#modal-login-only input[type="submit"]').trigger('click');


            // console.log('html', $('div#modal-login-only').html());
            // console.log('password', $('div#modal-login-only input#password'));
            // console.log('email', $('div#modal-login-only input#email'));
            // console.log('submit', $('div#modal-login-only input[type="submit"]'));


        }, loginInfoList);
    },
    function() {

        //STEP3 - POST LOGIN - READ RESULT LIST 
        console.log('POST LOGIN - READ RESULT LIST ');


        var fs = require('fs');
        var content = fs.read('./dist/data.txt');
        resultList = JSON.parse("[" + content + "{}" + "]");

    },
    function() {

        //STEP4 - FILL IN VALUES: UPLOAD TEXT 
        console.log('FILL IN VALUES: UPLOAD TEXT ');

 

        // Output content of page to stdout after form has been submitted
        page.evaluate(function(resultList, currentRowIndex, messageFormat) {
            //console.log('body html', $('body').html());
            // console.log(document.querySelectorAll('html')[0].outerHTML);

            // if (!resultList[currentRowIndex]) {
            //     return;
            // }

 
            //open box

            $('body').trigger('click');
            $('.dummy-composer-input').trigger('click');
            $('.dummy-composer-input').focus();
            $('.dummy-composer-input').focus();
            $('.dummy-composer-input').trigger('focus');


            /*
                        console.log('dashboard-composer length', $('.dashboard-composer').length);
                        console.log('sharer length', $('#sharer').length);
                        console.log('sharer html', $('#sharer').html());
                        console.log('composer length', $('#composer').length);
                        console.log('sharelater length', $('#share-later').length);
            */

            //test dummy key input

            $('#composer').focus();
            $('#share-later').removeAttr('disabled');
            $('#share-later').removeProp('disabled');


            console.log("  $('#share-later')",  $('#share-later') );
            // var valueToPrint = "By @" + resultList[currentRowIndex].username + "\n" + resultList[currentRowIndex].caption;
            var valueToPrint = messageFormat.replace(/<\w+>/g, function(placeholderKey) {
                placeholderKey = placeholderKey.replace(/</g, '').replace(/>/g, '');
                return resultList[currentRowIndex][placeholderKey] || placeholderKey;
            });
            console.log('valueToPrint', valueToPrint);

            $('#composer').val(valueToPrint);
            $('#composer').text(valueToPrint);
            $('#composer').prop("value", valueToPrint);

            setTimeout(function() {

                $('body').trigger('click');
                $('.dummy-composer-input').trigger('blur');
                $('.dummy-composer-input').trigger('click');
                $('.dummy-composer-input').trigger('focus');

                $('#composer').blur();
                $('#composer').focus();
                $('#composer').blur();
 
            }, 1500);
            // console.log( 'body html', $('body').html());


        }, resultList, currentRowIndex, messageFormat);
    },
    function() {
        //STEP5 - FILL IN VALUES: UPLOAD MEDIA 
        console.log('FILL IN VALUES: UPLOAD MEDIA ');

        if (!resultList[currentRowIndex]) {
            return;
        }
        var valueToPrint = "./dist/media/" + resultList[currentRowIndex].filename;
        console.log('...after open postshare box and filled in, time upload media.. :D ', valueToPrint);

        console.log('...from media path ', valueToPrint);

        page.uploadFile('input[name=file]', valueToPrint);

    },

    function() {

        //STEP6 - SUBMIT QUEUE POST
        console.log('SUBMIT QUEUE POST');
        // Output content of page to stdout after form has been submitted
        page.evaluate(function(resultList, currentRowIndex) {

            if (!resultList[currentRowIndex]) {
                return;
            }

                $('#share-later').trigger('click')
            // console.log('after insert: body html', $('body').html());
        }, resultList, currentRowIndex);


    },



    function() {

        //STEP7 - SIGNOUT
        console.log('SIGNOUT');

        // Output content of page to stdout after form has been submitted
        page.evaluate(function() {

            $('#signout-form').submit();
        });


    },




];


interval = setInterval(function() {
    var resultListLength = resultList.length - 2; // for exclude dummy length & machine index

 
    if (!loadInProgress && typeof steps[stepIndex] == "function") {




        console.log("STEP " + (stepIndex + 1));


        steps[stepIndex]();

        //repeat step 4-6 until resultList done
        //reset back STEP to make sure come in this loop

        if (stepIndex == 5 && currentRowIndex < resultListLength) {
            stepIndex = 2;

            currentRowIndex++;
        }
        stepIndex++;

    }
    if (typeof steps[stepIndex] != "function") {
        console.log("POST COMPLETED!");
        phantom.exit();
    }

}, globalTimeout);
