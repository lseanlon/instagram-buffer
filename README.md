# Instagram - buffer automation

#Installation
1)Download nodejs6.0.0
2)Download phantomjs2.0
3)Configure path environment variable /default path



# Run
1)npm install
2)sh downloadData.sh
3)sh uploadData.sh



To download data from instagram link, run 'sh download.sh', images and data will be auto downlaoded and scrapped into folder dist/.

To post upload data into buffer queue, run 'sh upload.sh', all resources will be added into queue.




# Usage
Configurations 

1) mantain all the global waiting timer in seconds
src/configuration/TimerInfo.txt

2)mantain all the instagram links here so it can download here
src/configuration/UrlListInfo.txt
 
3) mantain all the login credential here for buffer login
src/configuration/BufferLoginInfo.txt
 
4) mantain all the message formatting here for buffer post
src/configuration/BufferPostInfo.txt

possible placeholder currently
<username> 
<likes>
<caption>