var fs = require('fs');
var CommandParameter = require('./src/CommandParameter');
var ReadSourceUrl = require('./src/ReadSourceUrl');
var Scrap = require('./src/Scrap');
 
var inputStream = fs.createReadStream( CommandParameter('FILE') );

// var express = require('express');   
// var app     = express();  
// app.get('/', function(req, res) {
//     res.sendFile(  __dirname +  '/test/sampledata.html' );
// }); 
// app.listen(8000);


var urlList = [];
var readLineSuccessCallback = function(_lineList) {
    if (_lineList) { 
        console.log('urlList: ', _lineList);
         
        _lineList.forEach(function(_elemUrl) {
             console.log(_elemUrl);
            //add timeout - reduce suspicious of crawling
             setTimeout(function(){ 
                    //testing only-- please uncomment below
                    // _elemUrl = "http://localhost:8080";
                     Scrap(_elemUrl  );

             }, 3210); 
        });
 
            
    }
} 


ReadSourceUrl(inputStream, readLineSuccessCallback);


